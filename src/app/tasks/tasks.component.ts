import {Component, Directive, Input, ViewChild, OnInit, ElementRef} from '@angular/core';
import {FormControl,FormsModule, ReactiveFormsModule, FormGroup, NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition,} from '@angular/material';
import { Router } from '@angular/router';

import { TranslateString } from '../Strings';
import { Task } from '../Task';
import { Category } from '../Category';
import { Patient } from '../Patient';
import { Nurse } from '../Nurse';
import { Webservice } from '../Webservice';
import { Util } from '../Util';



@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  webservice = Webservice.getInstance();
  util = Util.getInstance();

  imageURL: string;
  imageNurseURL: string;
  selectedCategoryIcon : string;

  flagEditing = false;
  taskEditing : Task;
	

  translate = TranslateString.getInstance();

  delete_title_button: String;
  edit_title_button: String;
  cancel_title_button: String;
	show_hide_title_button: String;
	save_update_button_title: String;



	@ViewChild("newTaskForm", {static: false}) divView: ElementRef;
  @ViewChild("showHiddenButton", {static: false}) showHiddenButton: ElementRef;


  taskForm: FormGroup;
  categoriesControl: FormControl;


	searchForm: FormGroup;
  searchValue = '';

  	stateCtrl = new FormControl();


    patients : Observable<Patient[]>;
    nurses : Observable<Nurse[]>;

    selectedPatient :  Patient = null;
    selectedNurse :  Nurse = null;
    selectedCategory : string = null;





  	tasks : Observable<Task[]>;
  	filteredCategories: Observable<Category[]>;


  	categories: Category[] = [
    {
      title: 'Food',
      description: 'Breakfast, Lunch, Dinner, Snack',
      icon: 'assets/food_icon.png'
    },
    {
      title: 'Hygiene',
      description: 'Shower, Teeth brush...',
      icon: 'assets/shower_icon.png'
    },
    {
      title: 'Medication',
      description: 'Give patients medication',
      icon: 'assets/medication_icon.png'
    }
  ];

  constructor(private snackBar: MatSnackBar, private httpClient: HttpClient) {

  	
    this.filteredCategories = this.stateCtrl.valueChanges
      .pipe(
        startWith(''),
        map(category => category ? this._filterCategories(category) : this.categories.slice())
      );

  }

  private _filterCategories(value: string): Category[] {

    const filterValue = value.toLowerCase();

    return this.categories.filter(state => state.title.toLowerCase().indexOf(filterValue) === 0);
  }



  ngOnInit() {

    this.showAllTasks()
    this.loadAllPatients()
    this.loadAllNurses();

 
    this.taskForm = new FormGroup({
        titleControl: new FormControl(),
        patientControl: new FormControl(),
        categoriesControl: new FormControl(),
        nurseControl: new FormControl(),
        timeControl: new FormControl(),
        dateControl: new FormControl(),
        noteControl: new FormControl(),
        durationControl: new FormControl()

    });

    this.taskForm.controls['categoriesControl'].valueChanges.subscribe(value => {
       if (this.selectedCategory != null) {
            if (value != this.selectedCategory) {
                this.selectedCategory = null;
                this.selectedCategoryIcon = "";

            }

        }
      const filterValue = value.toLowerCase();
      this.categories.filter(state => state.title.toLowerCase().indexOf(filterValue) === 0);
    });

    this.taskForm.controls['patientControl'].valueChanges.subscribe(value => {
        console.log("value: "  + value);

        if (this.selectedPatient != null) {
            let name = this.selectedPatient.first_name + " " + this.selectedPatient.last_name;
            console.log("Selected patient name: " + name + " value: " + value);
            if (value != name) {
                this.selectedPatient = null;
                this.imageURL = "";

            }

        }
        
        this.filterPatientsByValue(value);
    });

     this.taskForm.controls['nurseControl'].valueChanges.subscribe(value => {

        if (this.selectedNurse != null) {
            let name = this.selectedNurse.first_name + " " + this.selectedNurse.last_name;
            if (value != name) {
                this.selectedNurse = null;
                this.imageNurseURL = "";

            }

        }

        this.filterNursesByValue(value);

    });

  	this.searchForm = new FormGroup({
        searchTask: new FormControl()
    
    });

    this.searchForm.controls['searchTask'].valueChanges.subscribe(value => {
        this.filterTasksBySearch(value);
    });

  	this.save_update_button_title = this.translate.getTranslatedString("NEW");
    this.delete_title_button  = this.translate.getTranslatedString("DELETE");
    this.edit_title_button  = this.translate.getTranslatedString("EDIT");
    this.show_hide_title_button = "add_circle_outline";
    this.cancel_title_button = this.translate.getTranslatedString("CANCEL");


  
  }

  ngAfterViewInit(){


    this.divView.nativeElement.style.display = "none";
  }

 filterPatientsByValue(search: String){
        console.log(search);

        if (search == "") { this.loadAllPatients() } else {

            this.patients = this.httpClient.get<Patient[]>('http://'+this.webservice.baseURL+'/patients/search/?search='+search)  

        }
  }

   filterNursesByValue(search: String){
        console.log(search);

        if (search == "") { this.loadAllNurses() } else {

            this.nurses = this.httpClient.get<Nurse[]>('http://'+this.webservice.baseURL+'/nurses/search/?search='+search)  

        }
  }
  



filterTasksBySearch(search: String){
        console.log(search);

        if (search == "") { this.showAllTasks() } else {

            this.tasks = this.httpClient.get<Task[]>('http://'+this.webservice.baseURL+'/tasks/search/?search='+search)  

        }
  }

  showAllTasks(){

     this.tasks = this.httpClient.get<Task[]>('http://'+this.webservice.baseURL+'/tasks')  

  }

  createOrUpdate(form: NgForm){

      console.log("New Task");
      console.log(this.taskForm.controls['titleControl'].value);

      var flagForm = true

      if (this.taskForm.controls['titleControl'].value == null) {
          console.log("need title");
          this.taskForm.controls['titleControl'].setErrors({'incorrect': true});
          flagForm = false
      }

       if (this.taskForm.controls['dateControl'].value == null) {
          this.taskForm.controls['dateControl'].setErrors({'incorrect': true});
          flagForm = false
      }

       if (this.taskForm.controls['timeControl'].value == null) {
          this.taskForm.controls['timeControl'].setErrors({'incorrect': true});
          flagForm = false
      }

       if (this.selectedCategory == null) {
          this.taskForm.controls['categoryControl'].setErrors({'incorrect': true});

          flagForm = false
      }

      if (this.selectedPatient == null) {
          console.log("Select patient");
          this.taskForm.controls['patientControl'].setErrors({'incorrect': true});

          flagForm = false
      }

      if (this.selectedNurse == null) {
          this.taskForm.controls['nurseControl'].setErrors({'incorrect': true});

          flagForm = false
      }



      if ( flagForm == false ) {return}


      let duration = ""

      if (this.taskForm.controls['durationControl'].value != null) {
          duration = this.taskForm.controls['timeControl'].value;
      }

      let note = ""

      if (this.taskForm.controls['noteControl'].value != null) {
          note = this.taskForm.controls['noteControl'].value;
      }





      let title =this.taskForm.controls['titleControl'].value
      let date = this.taskForm.controls['dateControl'].value
      let time = this.taskForm.controls['timeControl'].value
      let category = this.selectedCategory;
      let patient_id = this.selectedPatient._id;
      let nurse_id = this.selectedNurse._id;

      let parseDate = date.toDateString();
    


      const task = <Task>({

       nurse_id : nurse_id,
       patient_id : patient_id,
       timestamp : "",
       duration : duration,
       category: category,
       time: time,
       date: parseDate,
       completed: false,
       note: note,
       title: title

     });


      console.log(task)
    
    
     
    if (this.flagEditing) {
      task._id = this.taskEditing._id;
      this.update(task);
      this.flagEditing = false;
      this.taskEditing = null;
      return
    }

    this.createNew(task);


     

  }

  update(task: Task) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");


    this.httpClient.post('http://'+this.webservice.baseURL+'/tasks/'+task._id,task,{headers}).subscribe(
       val => {

            this.showAllTasks();
            this.closeForm();
            this.snackBar.open("Task was updated!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );

  }

  createNew(task: Task){
   
     const headers = new HttpHeaders().set("Content-Type", "application/json");

     this.httpClient.post('http://'+this.webservice.baseURL+'/tasks',task,{headers}).subscribe(
       val => {

            this.showAllTasks();
            this.closeForm();
            this.clearForm();
            this.snackBar.open("New task was created!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );

  }

 
 edit(task: Task, form: NgForm) {

        this.openForm()
        this.save_update_button_title = this.translate.getTranslatedString("Update Task");
        this.flagEditing = true;
        this.taskEditing = task;


      this.taskForm.controls['titleControl'].setValue(task.title);
      this.taskForm.controls['dateControl'].setValue(task.date);
      this.taskForm.controls['timeControl'].setValue(task.time);
      this.taskForm.controls['noteControl'].setValue(task.note);
      this.taskForm.controls['durationControl'].setValue(task.duration);

      this.selectNurse(task.nurse);
      this.selectPatient(task.patient);

      var i = 0;
      for (i; i<this.categories.length; i++) {
        let c = this.categories[i];
        if (c.title === task.category) {
            this.selectCategory(c)
            this.taskForm.controls['categoryControl'].setValue(task.category);

        }

      }


      

  }

  cancel(form: NgForm){
    this.save_update_button_title = this.translate.getTranslatedString("New Task");
    this.closeForm();
  }
  delete(task: Task) {
    var headers = new HttpHeaders()
    .set("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");


    this.httpClient.delete('http://'+this.webservice.baseURL+'/tasks/'+task._id,{headers}).subscribe(
       val => { this.showAllTasks()
                this.snackBar.open("Task was deleted!", "", {
                  duration: 2000,
                });
        },
       response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed."); }
    );
  }

  loadAllPatients(){
    this.patients = this.httpClient.get<Patient[]>('http://'+this.webservice.baseURL+'/patients');
  }

  loadAllNurses(){
     this.nurses = this.httpClient.get<Nurse[]>('http://'+this.webservice.baseURL+'/nurses');
  }

  toogleForm(){

     if (this.divView.nativeElement.style.display === "none") {
          this.divView.nativeElement.style.display = "block";
          this.show_hide_title_button = "remove_circle_outline";

      } else {
         this.divView.nativeElement.style.display = "none";
         this.show_hide_title_button = "add_circle_outline";

      }

  }

  openForm(){

    this.divView.nativeElement.style.display = "block";
    this.show_hide_title_button = "remove_circle_outline";
  }

  closeForm(){

    this.divView.nativeElement.style.display = "none";
    this.show_hide_title_button = "add_circle_outline";
  }

  clearForm(){
    this.save_update_button_title = this.translate.getTranslatedString("New Task");

      this.taskForm.controls['titleControl'].setValue("");
      this.taskForm.controls['dateControl'].setValue("");
      this.taskForm.controls['timeControl'].setValue("");
      this.taskForm.controls['noteControl'].setValue("");
      this.taskForm.controls['durationControl'].setValue("");
      this.taskForm.controls['nurseControl'].setValue("");
      this.taskForm.controls['patientControl'].setValue("");
      this.selectedPatient = null;
      this.selectedNurse = null;
      this.selectedCategoryIcon = "";
      this.selectedCategory = null;

  }

  selectCategory(category: Category) {
    this.selectedCategory = category.title;
    this.selectedCategoryIcon = this.getCategoryIcon(category.title)

   // this.imageURL = this.getProfilePicture(patient._id) as string;


    console.log("Patient: " + name + " selected");
     this.taskForm.controls['categoryControl'].setValue(category.title);
  }

  selectPatient(patient: Patient) {
    let name = patient.first_name + " " + patient.last_name;
    this.selectedPatient = patient;

    this.imageURL = this.getProfilePicture(patient._id) as string;


    console.log("Patient: " + name + " selected");
     this.taskForm.controls['patientControl'].setValue(name);

    //form.controls['title'].setValue(name);

  }

 selectNurse(nurse: Nurse) {
    let name = nurse.first_name + " " + nurse.last_name;
    this.selectedNurse = nurse;

    this.imageNurseURL = this.getProfilePicture(nurse._id) as string;


     this.taskForm.controls['nurseControl'].setValue(name);
  }
   getProfilePicture(id: String): String {
    return "http://"+this.webservice.baseURL+"/pictures/" + id + ".png";
  }

  getCategoryIcon(category: String): string {




    if (category == "Food") { return "restaurant" };
    if (category == "Washroom") { return "wc" };
    if (category == "Hygiene") { return "wc" };
    if (category == "Medication") { return "local_hospital" };
    if (category == "Medicine") { return "local_hospital" };
    if (category == "Checkup") { return "done_outline" };


    return ""

  }

  select(task: Task) {
    console.log(task);
  }

   mockup(form: NgForm) {

      this.taskForm.controls['titleControl'].setValue("Title mockup debug web");
      this.taskForm.controls['dateControl'].setValue("12/12/2019");
      this.taskForm.controls['timeControl'].setValue("10 am");
      this.taskForm.controls['noteControl'].setValue("This is a test");
      this.taskForm.controls['durationControl'].setValue("10 min");


  }

}
