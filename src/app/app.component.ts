import { Component, OnInit } from '@angular/core';
import { TranslateString } from './Strings';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'avencare-web';
  translate = TranslateString.getInstance();

  menuTitleHome :String = "Home"
  menuTitlePatients :String = "Patients"
  menuTitleNurses :String = "Nurses"
  menuTitleTasks :String = "Tasks"
  menuTitleReports :String = "Reports"

  ngOnInit() {
   this.menuTitleHome = this.translate.getTranslatedString("Home");
    this.menuTitlePatients = this.translate.getTranslatedString("Patients");
    this.menuTitleNurses = this.translate.getTranslatedString("Nurses");
    this.menuTitleTasks = this.translate.getTranslatedString("Tasks");
    this.menuTitleReports = this.translate.getTranslatedString("Reports");

  } 
  setLanguage(language: string){

  	console.log("change language: " + language);
  	this.translate.setLanguage(language)
  	window.location.reload();

   

  }
}
