import { Webservice } from './Webservice';


export class Util {
  
  private static instance: Util;
  webservice = Webservice.getInstance();


  private constructor() {
  }

  static getInstance(): Util {
    if (!Util.instance) {
      Util.instance = new Util();

    }

    return Util.instance;
  }

  public getHeader(string: String): String {

    return string;
  }

   getProfilePicture(id: String): String {
    return "http://"+this.webservice.baseURL+"/pictures/" + id + ".png";
  }

}