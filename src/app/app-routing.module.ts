import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksComponent } from './tasks/tasks.component';
import { PatientsComponent } from './patients/patients.component';
import { ReportsComponent } from './reports/reports.component';
import { NursesComponent } from './nurses/nurses.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';



const routes: Routes = [
    {path:'',redirectTo:'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
	{ path: 'tasks', component: TasksComponent },
	{ path: 'nurses', component: NursesComponent },
	{ path: 'patients', component: PatientsComponent },
	{ path: 'reports', component: ReportsComponent },
    { path: 'home', component: HomeComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
