import {Component, Directive, Input, ViewChild, OnInit, ElementRef} from '@angular/core';
import {FormControl,FormsModule, ReactiveFormsModule, FormGroup, NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { MatExpansionModule, MatButtonModule } from '@angular/material';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition,} from '@angular/material';
import { Router } from '@angular/router';

import { TranslateString } from '../Strings';
import { Task } from '../Task';
import { Category } from '../Category';
import { Patient } from '../Patient';
import { Nurse } from '../Nurse';
import { Webservice } from '../Webservice';
import { Util } from '../Util';


@Component({
  selector: 'app-new-patient',
  templateUrl: './new-patient.component.html',
  styleUrls: ['./new-patient.component.css']
})
export class NewPatientComponent implements OnInit {

	webservice = Webservice.getInstance();
  	util = Util.getInstance();
  	translate = TranslateString.getInstance();


	newPatientForm: FormGroup;

	selectedNewPatientProfileURL : ArrayBuffer
 	selectedNewPatientProfilePicture: File



   	constructor(private snackBar: MatSnackBar, private httpClient: HttpClient) { }

  	ngOnInit() {

  		this.newPatientForm = new FormGroup({

  			firstNameControl: new FormControl(),
		    lastNameControl: new FormControl(),
		    dateOfBirthControl: new FormControl(),
		    sexControl: new FormControl(),
		    heightControl: new FormControl(),
		    weightControl: new FormControl(),
        	addresControl: new FormControl(),
        	cityControl: new FormControl(),
        	zipCodeControl: new FormControl(),
        	emergencyContactControl: new FormControl(),
        	emergencyContactRelationshipControl: new FormControl(),
        	emergencyContactNumberControl: new FormControl(),
        	emailControl: new FormControl(),
        	roomControl: new FormControl(),
        	conditionControl: new FormControl(),
        	noteControl: new FormControl()

    	});
  
  	}


  	create(panel: any){

  		//validation

  		

  		 let firstName = this.newPatientForm.controls['firstNameControl'].value
  		 let lastName = this.newPatientForm.controls['lastNameControl'].value
  		 let dateOfBirth = this.newPatientForm.controls['dateOfBirthControl'].value
  		 let sex = this.newPatientForm.controls['sexControl'].value
  		 let height = this.newPatientForm.controls['heightControl'].value
  		 let weight = this.newPatientForm.controls['weightControl'].value
  		 let addres = this.newPatientForm.controls['addresControl'].value
  		 let city = this.newPatientForm.controls['cityControl'].value
  		 let zipCode = this.newPatientForm.controls['zipCodeControl'].value
  		 let emergencyContact = this.newPatientForm.controls['emergencyContactControl'].value
  		 let emergencyContactRelationship = this.newPatientForm.controls['emergencyContactRelationshipControl'].value
  		 let emergencyContactNumber = this.newPatientForm.controls['emergencyContactNumberControl'].value
  		 let email = this.newPatientForm.controls['emailControl'].value
  		 let room = this.newPatientForm.controls['roomControl'].value
  		 let condition = this.newPatientForm.controls['conditionControl'].value
  		 let note = this.newPatientForm.controls['noteControl'].value

  		 const patient = <Patient>({
    		first_name: firstName,
   			last_name: lastName,
   			date_of_birth: dateOfBirth,
   			sex: sex,
   			height: height,
   			weight: weight,
   			address: addres,
   			city: city,
   			zip_code: zipCode,
			  emergency_contact: emergencyContact,
   			emergency_contact_relationship: emergencyContactRelationship,
   			emergency_contact_number: emergencyContactNumber,
   			email: email,
   			room: room,
        	condition: condition,
   			note: note,

		 });

  		 this.createNewPatient(patient,panel)

		 
    
    
   }

   createNewPatient(patient: Patient, panel: any) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");


    this.httpClient.post('http://'+this.webservice.baseURL+'/patients',patient,{headers}).subscribe(
       val => {

       		let newPatient : Patient = <Patient>val;
       		console.log("check pkicture");
       		console.log("response: " + newPatient._id);

       		if (this.selectedNewPatientProfileURL && this.selectedNewPatientProfilePicture) {
       			this.uploadImage(this.selectedNewPatientProfilePicture,newPatient._id).subscribe(
         		(res) => {
           				console.log(res);
        		 },
          		(err) => {
            			console.log(err);
       			 })

       		}

			this.newPatientForm.reset();
  			panel.close()
            this.snackBar.open("New patient was created!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );

  }

  	onSelectFilePictureNewPatient(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.selectedNewPatientProfilePicture = event.target.files[0];

      reader.readAsDataURL(this.selectedNewPatientProfilePicture); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.selectedNewPatientProfileURL = <ArrayBuffer>reader.result;

      }
    }
  }

  removeFilePictureNewPatient(event) {
      this.selectedNewPatientProfileURL = null
  }

  public uploadImage(image: File, id: string): Observable<Object> {
    const formData = new FormData();
    console.log("upload pic");
    formData.append('picture', image);
    return this.httpClient.post('http://'+this.webservice.baseURL+'/pictures/' + id,formData)
  }

  	mockup(){

  	const patient = <any>({
         first_name: "Patrick",
         last_name: "Stewart",
         date_of_birth: "07/13/1940",
         sex: "M",
         height: "178",
         weight: "72",
         address: "1 Austin Terrace",
         city: "Toronto",
         zip_code: "M5R 1X8",
         emergency_contact: "Sunny Ozell",
         emergency_contact_relationship: "Spouse",
         emergency_contact_number: "416 666 1452",
         email: "patrick.stewart@yahoo.com",
         room: "A102",
         condition: "Fair",
         note: "Very quiet"
    });

      this.newPatientForm.controls['firstNameControl'].setValue(patient.first_name);
      this.newPatientForm.controls['lastNameControl'].setValue(patient.first_name);
      this.newPatientForm.controls['dateOfBirthControl'].setValue(patient.date_of_birth);
      this.newPatientForm.controls['sexControl'].setValue(patient.sex);
      this.newPatientForm.controls['heightControl'].setValue(patient.height);
      this.newPatientForm.controls['weightControl'].setValue(patient.weight);
      this.newPatientForm.controls['addresControl'].setValue(patient.address);
      this.newPatientForm.controls['cityControl'].setValue(patient.city);
      this.newPatientForm.controls['zipCodeControl'].setValue(patient.zip_code);
      this.newPatientForm.controls['emergencyContactControl'].setValue(patient.emergency_contact);
      this.newPatientForm.controls['emergencyContactRelationshipControl'].setValue(patient.emergency_contact_relationship);
      this.newPatientForm.controls['emergencyContactNumberControl'].setValue(patient.emergency_contact_number);
      this.newPatientForm.controls['emailControl'].setValue(patient.email);
      this.newPatientForm.controls['roomControl'].setValue(patient.room);
      this.newPatientForm.controls['conditionControl'].setValue(patient.room);
      this.newPatientForm.controls['noteControl'].setValue(patient.note);

  }
  	

}

















