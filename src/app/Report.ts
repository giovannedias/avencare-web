import { Task } from './Task';
import { Patient } from './Patient';
import { Nurse } from './Nurse';


export interface Reading {
        _id: string;
       name: string;
       result: string;
}

export interface Report {
  _id: string;
  patient_id: string;
  nurse_id: string;
  task_id:string;
  timestamp: string;
  note: string;
  task: Task;
  patient: Patient;
  nurse: Nurse;
  readings: [ Reading ];
}

