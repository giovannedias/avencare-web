import {Component, Directive, Input, ViewChild, OnInit, ElementRef} from '@angular/core';
import {FormControl,FormsModule, ReactiveFormsModule, FormGroup, NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition,} from '@angular/material';
import { Router } from '@angular/router';

import { TranslateString } from '../Strings';
import { Task } from '../Task';
import { Patient } from '../Patient';
import { Report, Reading } from '../Report';
import { Webservice } from '../Webservice';
import { Util } from '../Util';


@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  webservice = Webservice.getInstance();
  util = Util.getInstance();

	searchForm: FormGroup;
  	searchValue = '';

 	reports : Observable<Report[]>;

  	translate = TranslateString.getInstance();

 	delete_title_button: String;
  edit_title_button: String;
  cancel_title_button: String;
	show_hide_title_button: String;
	save_update_button_title: String;

  constructor(private snackBar: MatSnackBar, private httpClient: HttpClient) {


  }



  ngOnInit() {

  	this.searchForm = new FormGroup({
        searchTask: new FormControl()
    
    });

    this.searchForm.controls['searchTask'].valueChanges.subscribe(value => {
        this.filterSearchByValue(value);
    });

    this.save_update_button_title = this.translate.getTranslatedString("New Patient");
    this.delete_title_button  = this.translate.getTranslatedString("DELETE");
    this.edit_title_button  = this.translate.getTranslatedString("EDIT");
    this.cancel_title_button = this.translate.getTranslatedString("CANCEL");


    this.show_hide_title_button = "keyboard_arrow_down";

    this.showAllReports()
  }

  
  filterSearchByValue(search: String){
        console.log(search);

        if (search == "") { this.showAllReports() } else {

            this.reports = this.httpClient.get<Report[]>('http://'+this.webservice.baseURL+'/reports/search/?search='+search)  

        }
  }

  select(report: Report, view : HTMLInputElement) {
    console.log(view);
     this.toogleView(view);
  }

  showAllReports(){

     this.reports = this.httpClient.get<Report[]>('http://'+this.webservice.baseURL+'/reports')  
  }

  delete(report: Report) {
    var headers = new HttpHeaders()
    .set("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");


    this.httpClient.delete('http://'+this.webservice.baseURL+'/reports/'+report._id,{headers}).subscribe(
       val => { this.showAllReports()
                this.snackBar.open("Report was deleted!", "", {
                  duration: 2000,
                });
        },
       response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed."); }
    );
  }
  

  getProfilePicture(id: String): String {
    return "http://"+this.webservice.baseURL+"/pictures/" + id + ".png";
  }

  getCategoryIcon(category: String): String {

    return "done_outline"

    var icon = "food_icon.jpg"

    if (category == "Food") { return "restaurant"}
    if (category == "Washroom") { return "bathtube"}
    if (category == "Medicine") { return "local_hospital"}
    if (category == "Checkup") { return "done_outline"}





    return "food_icon.jpg";
  }

  getDateFromTimestamp(timestamp: string) {

    console.log("timestamp: " + timestamp);
    var date = new Date(timestamp);
    return date
  }


  toogleView(view: HTMLInputElement){

     if (view.style.display === "none") {
          view.style.display = "block";
         this.show_hide_title_button = "keyboard_arrow_up"
      } else {
         view.style.display = "none";
         this.show_hide_title_button = "keyboard_arrow_down"
      }

  }

  


}


