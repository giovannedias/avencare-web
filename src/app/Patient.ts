export interface Patient {
  _id: string;
  first_name: string;
  last_name: string;
  date_of_birth: string;
  sex: string;
  height: number;
  weight: number;
  address: string;
  city: string;
  zip_code: string;
  emergency_contact: string;
  emergency_contact_relationship: string;
  emergency_contact_number: string;
  email: string;
  room: string;
  condition: string;
  note: string;
}