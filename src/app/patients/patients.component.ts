import {Component, Directive, Input, ViewChild, OnInit, ElementRef} from '@angular/core';
import {FormControl,FormsModule, ReactiveFormsModule, FormGroup, NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition,} from '@angular/material';
import { Router } from '@angular/router';

import { TranslateString } from '../Strings';
import { Task } from '../Task';
import { Patient } from '../Patient';
import { Webservice } from '../Webservice';
import { Util } from '../Util';


@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {


  webservice = Webservice.getInstance();
  util = Util.getInstance();

  condition = "GOOD";



  flagEditing = false;
  patientEditing : Patient;

	searchForm: FormGroup;
  searchValue = '';

  patients : Observable<Patient[]>;

  translate = TranslateString.getInstance();

  delete_title_button: String;
  edit_title_button: String;
  cancel_title_button: String;
	show_hide_title_button: String;
	save_update_button_title: String;
  more_info_title_button : String;


  @ViewChild("newTaskForm", {static: false}) divView: ElementRef;
  @ViewChild("showHiddenButton", {static: false}) showHiddenButton: ElementRef;


  constructor(private snackBar: MatSnackBar, private httpClient: HttpClient) {


  }

  ngOnInit() {

  	this.searchForm = new FormGroup({
        searchTask: new FormControl()
    
    });

    this.searchForm.controls['searchTask'].valueChanges.subscribe(value => {
        this.filterSearchByValue(value);
    });

    this.save_update_button_title = this.translate.getTranslatedString("NEW");
    this.delete_title_button  = this.translate.getTranslatedString("DELETE");
    this.edit_title_button  = this.translate.getTranslatedString("EDIT");
    this.cancel_title_button = this.translate.getTranslatedString("CANCEL");

    this.more_info_title_button = "keyboard_arrow_down"

    this.show_hide_title_button = "add_circle_outline";

    this.showAllPatients()

  }

  ngAfterViewInit(){
    this.divView.nativeElement.style.display = "none";
  }

  filterSearchByValue(search: String){
        console.log(search);

        if (search == "") { this.showAllPatients() } else {

            this.patients = this.httpClient.get<Patient[]>('http://'+this.webservice.baseURL+'/patients/search/?search='+search)  

        }
  }

  select(patient: Patient, view : HTMLInputElement) {
    console.log("Patient selected: " + patient.first_name + " " + patient.last_name + " id: " + patient._id);
    this.toogleView(view);

  }
  
  showAllPatients(){

     this.patients = this.httpClient.get<Patient[]>('http://'+this.webservice.baseURL+'/patients')  
  }

  createOrUpdate(form: NgForm){

    
    const patient = <Patient>({
    		first_name: form.value.first_name,
   			last_name: form.value.last_name,
   			date_of_birth: form.value.date_of_birth,
   			sex: form.value.sex,
   			height: form.value.height,
   			weight: form.value.weight,
   			address: form.value.address,
   			city: form.value.city,
   			zip_code: form.value.zip_code,
			  emergency_contact: form.value.emergency_contact,
   			emergency_contact_relationship: form.value.emergency_contact_relationship,
   			emergency_contact_number: form.value.emergency_contact_number,
   			email: form.value.email,
   			room: form.value.room,
        condition: form.value.condition,
   			note: form.value.note,

		 });

    if (this.flagEditing) {
      patient._id = this.patientEditing._id;
      this.updatePatient(patient);
      this.flagEditing = false;
      this.patientEditing = null;
      return
    }

    this.createNewPatient(patient);

  }

  updatePatient(patient: Patient) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    console.log("ID: " + patient._id + " name: " + patient.first_name);
     


    this.httpClient.post('http://'+this.webservice.baseURL+'/patients/'+ patient._id,patient,{headers}).subscribe(
       val => {

            this.showAllPatients();
            this.closeForm();
            this.snackBar.open("New patient was updated!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );
  }

  createNewPatient(patient: Patient) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");


    this.httpClient.post('http://'+this.webservice.baseURL+'/patients',patient,{headers}).subscribe(
       val => {

            this.showAllPatients();
            this.closeForm();
            this.snackBar.open("New patient was created!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );

  }


  delete(patient: Patient) {
    var headers = new HttpHeaders()
    .set("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");


    this.httpClient.delete('http://'+this.webservice.baseURL+'/patients/'+patient._id,{headers}).subscribe(
       val => { this.showAllPatients()
                this.snackBar.open("Patient was deleted!", "", {
                  duration: 2000,
                });
        },
       response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed."); }
    );
  }

  edit(patient: Patient, form: NgForm) {

        this.openForm()
        this.save_update_button_title = this.translate.getTranslatedString("Update Patient");
        this.flagEditing = true;
        this.patientEditing = patient;


        form.controls['first_name'].setValue(patient.first_name);
        form.controls['last_name'].setValue(patient.last_name);
        form.controls['date_of_birth'].setValue(patient.date_of_birth);
        form.controls['sex'].setValue(patient.sex);
        form.controls['height'].setValue(patient.height);
        form.controls['weight'].setValue(patient.weight);
        form.controls['address'].setValue(patient.address);
        form.controls['city'].setValue(patient.city);
        form.controls['zip_code'].setValue(patient.zip_code);
        form.controls['emergency_contact'].setValue(patient.emergency_contact);
        form.controls['emergency_contact_relationship'].setValue(patient.emergency_contact_relationship);
        form.controls['emergency_contact_number'].setValue(patient.emergency_contact_number);
        form.controls['email'].setValue(patient.email);
        form.controls['room'].setValue(patient.room);
        form.controls['condition'].setValue(patient.condition);
        form.controls['note'].setValue(patient.note);

  }

  cancel(form: NgForm){

    this.save_update_button_title = this.translate.getTranslatedString("New Patient");

    this.closeForm();
  }


  toogleForm(){

    console.log("toogle form");

     if (this.divView.nativeElement.style.display === "none") {
          this.divView.nativeElement.style.display = "block";
          this.show_hide_title_button = "remove_circle_outline";
      } else {
          this.divView.nativeElement.style.display = "none";
          this.show_hide_title_button = "add_circle_outline";
      }

  }

  openForm(){

    this.divView.nativeElement.style.display = "block";
    this.show_hide_title_button = "remove_circle_outline";
  }

  closeForm(){

    this.divView.nativeElement.style.display = "none";
    this.show_hide_title_button = "add_circle_outline";
  }

   toogleView(view: HTMLInputElement){

     if (view.style.display === "none") {
          view.style.display = "block";
         this.more_info_title_button = "keyboard_arrow_up"
      } else {
         view.style.display = "none";
         this.more_info_title_button = "keyboard_arrow_down"
      }

  }


  getProfilePicture(id: String): String {
    return "http://"+this.webservice.baseURL+"/pictures/" + id + ".png";
  }

  mockup(form: NgForm) {

    const PatrickStewart = <any>({
        first_name: "Patrick",
         last_name: "Stewart",
         date_of_birth: "07/13/1940",
         sex: "M",
         height: "178",
         weight: "72",
         address: "1 Austin Terrace",
         city: "Toronto",
         zip_code: "M5R 1X8",
         emergency_contact: "Sunny Ozell",
         emergency_contact_relationship: "Spouse",
         emergency_contact_number: "416 666 1452",
         email: "patrick.stewart@yahoo.com",
         room: "A102",
         condition: "Fair",
         note: "Very quiet"
    });


    let patient = <Patient>PatrickStewart

    form.controls['first_name'].setValue(patient.first_name);
        form.controls['last_name'].setValue(patient.last_name);
        form.controls['date_of_birth'].setValue(patient.date_of_birth);
        form.controls['sex'].setValue(patient.sex);
        form.controls['height'].setValue(patient.height);
        form.controls['weight'].setValue(patient.weight);
        form.controls['address'].setValue(patient.address);
        form.controls['city'].setValue(patient.city);
        form.controls['zip_code'].setValue(patient.zip_code);
        form.controls['emergency_contact'].setValue(patient.emergency_contact);
        form.controls['emergency_contact_relationship'].setValue(patient.emergency_contact_relationship);
        form.controls['emergency_contact_number'].setValue(patient.emergency_contact_number);
        form.controls['email'].setValue(patient.email);
        form.controls['room'].setValue(patient.room);
        form.controls['condition'].setValue(patient.condition);
        form.controls['note'].setValue(patient.note);

    

  }

}
