export interface Nurse {
  _id: string;
  first_name: string;
  last_name: string;
  register_nurse_id: string;
  shift: string;
  admin: boolean;
  password: string;
  date_of_birth: string;
  address: string;
  city: string;
  zip_code: string;
  number: string;
  email: string;

}