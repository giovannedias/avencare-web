import {Component, Directive, Input, ViewChild, OnInit, ElementRef} from '@angular/core';
import {FormControl,FormsModule, ReactiveFormsModule, FormGroup, NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition,} from '@angular/material';
import { Router } from '@angular/router';

import { TranslateString } from '../Strings';
import { Task } from '../Task';
import { Patient } from '../Patient';
import { Nurse } from '../Nurse';
import { Webservice } from '../Webservice';
import { Util } from '../Util';

export interface Shift {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-nurses',
  templateUrl: './nurses.component.html',
  styleUrls: ['./nurses.component.css']
})
export class NursesComponent implements OnInit {

  webservice = Webservice.getInstance();
  util = Util.getInstance();

  flagEditing = false;
  nurseEditing : Nurse;

  nurseForm: FormGroup;


  searchForm: FormGroup;
  searchValue = '';

  nurses : Observable<Nurse[]>;

  translate = TranslateString.getInstance();

  delete_title_button: String;
  edit_title_button: String;
  cancel_title_button: String;
  show_hide_title_button: String;
  save_update_button_title: String;
  more_info_title_button : String;

  @ViewChild("newTaskForm", {static: false}) divView: ElementRef;
  @ViewChild("showHiddenButton", {static: false}) showHiddenButton: ElementRef;

  shifts: Shift[] = [
    {value: 'Morning', viewValue: 'Morning'},
    {value: 'Afternoon', viewValue: 'Afternoon'},
    {value: 'Night', viewValue: 'Night'}
  ];

  constructor(private snackBar: MatSnackBar, private httpClient: HttpClient) {


  }

  ngOnInit() {

  	this.nurseForm = new FormGroup({
        firstNameControl: new FormControl(),
		    lastNameControl: new FormControl(),
		    registerNurseIDControl: new FormControl(),
		    shiftControl: new FormControl(),
		    adminControl: new FormControl(),
		    dateOfBirthControl: new FormControl(),
        addresControl: new FormControl(),
        cityControl: new FormControl(),
        zipCodeControl: new FormControl(),
        numberControl: new FormControl(),
        emailControl: new FormControl()
    });

  	this.searchForm = new FormGroup({
        searchTask: new FormControl()
    
    });

    this.searchForm.controls['searchTask'].valueChanges.subscribe(value => {
        this.filterSearchByValue(value);
    });

    this.save_update_button_title = this.translate.getTranslatedString("NEW");
    this.delete_title_button  = this.translate.getTranslatedString("DELETE");
    this.edit_title_button  = this.translate.getTranslatedString("EDIT");
    this.cancel_title_button = this.translate.getTranslatedString("CANCEL");

    this.more_info_title_button = "keyboard_arrow_down"

    this.show_hide_title_button = "add_circle_outline";

    this.showAllNurses()

  }

   ngAfterViewInit(){
    this.divView.nativeElement.style.display = "none";
  }

  filterSearchByValue(search: String){
        console.log(search);

        if (search == "") { this.showAllNurses() } else {

            this.nurses = this.httpClient.get<Nurse[]>('http://'+this.webservice.baseURL+'/nurses/search/?search='+search)  

        }
  }

  select(nurse: Nurse, view : HTMLInputElement) {
    console.log("Nurse selected: " + nurse.first_name + " " + nurse.last_name + " id: " + nurse._id);
    this.toogleView(view);

  }

  showAllNurses(){

     this.nurses = this.httpClient.get<Nurse[]>('http://'+this.webservice.baseURL+'/nurses')  
  }

  createOrUpdate(){
    
    const nurse = <Nurse>({
    		first_name: this.nurseForm.controls['firstNameControl'].value,
   			last_name: this.nurseForm.controls['lastNameControl'].value,
        register_nurse_id: this.nurseForm.controls['registerNurseIDControl'].value,
        shift: this.nurseForm.controls['shiftControl'].value,
   			date_of_birth: this.nurseForm.controls['dateOfBirthControl'].value,
   			address: this.nurseForm.controls['addresControl'].value,
   			city: this.nurseForm.controls['cityControl'].value,
   			zip_code: this.nurseForm.controls['zipCodeControl'].value,
			  number: this.nurseForm.controls['numberControl'].value,
   			email: this.nurseForm.controls['emailControl'].value
		 });

    if (this.flagEditing) {
      nurse._id = this.nurseEditing._id;
      this.update(nurse);
      this.flagEditing = false;
      this.nurseEditing = null;
      return
    }

    this.createNew(nurse);

  }

  update(nurse: Nurse) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");

    console.log("Updating : id" + nurse._id + " name: " + nurse.first_name) ;


    this.httpClient.post('http://'+this.webservice.baseURL+'/nurses/'+nurse._id,nurse,{headers}).subscribe(
       val => {

            this.showAllNurses();
            this.closeForm();
            this.snackBar.open("Nurse was updated!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );
  }

  createNew(nurse: Nurse) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");


    this.httpClient.post('http://'+this.webservice.baseURL+'/nurses',nurse,{headers}).subscribe(
       val => {

            this.showAllNurses();
            this.closeForm();
            this.snackBar.open("New patient was created!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );
  }

  delete(nurse: Nurse) {
    var headers = new HttpHeaders()
    .set("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");


    this.httpClient.delete('http://'+this.webservice.baseURL+'/nurses/'+nurse._id,{headers}).subscribe(
       val => { this.showAllNurses()
                this.snackBar.open("Patient was deleted!", "", {
                  duration: 2000,
                });
        },
       response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed."); }
    );
  }

  edit(nurse: Nurse, form: NgForm) {

        this.openForm()
        this.save_update_button_title = this.translate.getTranslatedString("Update Patient");
        this.flagEditing = true;
        this.nurseEditing = nurse;

        this.nurseForm.controls['firstNameControl'].setValue(nurse.first_name);
      this.nurseForm.controls['lastNameControl'].setValue(nurse.last_name);
      this.nurseForm.controls['registerNurseIDControl'].setValue(nurse.register_nurse_id);
      //this.nurseForm.controls['shiftControl'].setValue(moringShift.viewValue, {onlySelf: true});
      //this.nurseForm.controls['adminControl'].setValue("10 min");
      this.nurseForm.controls['dateOfBirthControl'].setValue(nurse.date_of_birth);
      this.nurseForm.controls['addresControl'].setValue(nurse.address);
      this.nurseForm.controls['cityControl'].setValue(nurse.city);
      this.nurseForm.controls['zipCodeControl'].setValue(nurse.zip_code);
      this.nurseForm.controls['numberControl'].setValue(nurse.number);
      this.nurseForm.controls['emailControl'].setValue(nurse.email);


        /*form.controls['first_name'].setValue(patient.first_name);
        form.controls['last_name'].setValue(patient.last_name);
        form.controls['date_of_birth'].setValue(patient.date_of_birth);
        form.controls['sex'].setValue(patient.sex);
        form.controls['height'].setValue(patient.height);
        form.controls['weight'].setValue(patient.weight);
        form.controls['address'].setValue(patient.address);
        form.controls['city'].setValue(patient.city);
        form.controls['zip_code'].setValue(patient.zip_code);
        form.controls['emergency_contact'].setValue(patient.emergency_contact);
        form.controls['emergency_contact_relationship'].setValue(patient.emergency_contact_relationship);
        form.controls['emergency_contact_number'].setValue(patient.emergency_contact_number);
        form.controls['email'].setValue(patient.email);
        form.controls['room'].setValue(patient.room);
        form.controls['condition'].setValue(patient.condition);
        form.controls['note'].setValue(patient.note);*/

  }

  cancel(form: NgForm){

    this.save_update_button_title = this.translate.getTranslatedString("New Patient");

    this.closeForm();
  }


  toogleForm(){

    console.log("toogle form");

     if (this.divView.nativeElement.style.display === "none") {
          this.divView.nativeElement.style.display = "block";
          this.show_hide_title_button = "remove_circle_outline";
      } else {
          this.divView.nativeElement.style.display = "none";
          this.show_hide_title_button = "add_circle_outline";
      }

  }

  openForm(){

    this.divView.nativeElement.style.display = "block";
    this.show_hide_title_button = "remove_circle_outline";
  }

  closeForm(){

    this.divView.nativeElement.style.display = "none";
    this.show_hide_title_button = "add_circle_outline";
  }

   toogleView(view: HTMLInputElement){

     if (view.style.display === "none") {
          view.style.display = "block";
         this.more_info_title_button = "keyboard_arrow_up"
      } else {
         view.style.display = "none";
         this.more_info_title_button = "keyboard_arrow_down"
      }

  }


  getProfilePicture(id: String): String {
    return "http://"+this.webservice.baseURL+"/pictures/" + id + ".png";
  }

 mockup() {

    let moringShift = this.shifts[0];
    console.log(moringShift)
    this.nurseForm.controls['firstNameControl'].setValue("Abby");
      this.nurseForm.controls['lastNameControl'].setValue("Lockhart");
      this.nurseForm.controls['registerNurseIDControl'].setValue("RN100123123");
      this.nurseForm.controls['shiftControl'].setValue(moringShift.viewValue, {onlySelf: true});
      //this.nurseForm.controls['adminControl'].setValue("10 min");
      this.nurseForm.controls['dateOfBirthControl'].setValue("02/03/1965");
      this.nurseForm.controls['addresControl'].setValue("75 Sundance Crescent");
      this.nurseForm.controls['cityControl'].setValue("Scarborough");
      this.nurseForm.controls['zipCodeControl'].setValue("M1G 2M1");
      this.nurseForm.controls['numberControl'].setValue("416 123 0109");
      this.nurseForm.controls['emailControl'].setValue("abby.lockhart@gmail.com");


}


}
