import { Patient } from './Patient';
import { Nurse } from './Nurse';

export interface Task {
  _id: string;
  nurse_id: string;
  patient_id: string;
  timestamp: string;
  duration: string;
  category: string;
  time: string;
  date: string;
  note: string;
  completed: boolean;
  title: string;
  patient: Patient;
  nurse: Nurse;

}

