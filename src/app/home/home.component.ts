import {Component, Directive, Input, ViewChild, OnInit, ElementRef} from '@angular/core';
import {FormControl,FormsModule, ReactiveFormsModule, FormGroup, NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition,} from '@angular/material';
import { Router } from '@angular/router';

import { TranslateString } from '../Strings';
import { Task } from '../Task';
import { Category } from '../Category';
import { Patient } from '../Patient';
import { Nurse } from '../Nurse';
import { Webservice } from '../Webservice';
import { Util } from '../Util';
import { Alarm } from '../Alarm';





export interface Room {
  number: string;
  occupy: boolean;
  patient_id: string;
  status: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  patients : Observable<Patient[]>;


  alarmForm: FormGroup;
  webservice = Webservice.getInstance();
  util = Util.getInstance();


  rooms: Room[] = [
    {
      number: 'A001',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    },
    {
      number: 'Food',
      occupy: true,
      patient_id: '',
      status: ''
    }
  ];

   constructor(private snackBar: MatSnackBar, private httpClient: HttpClient) {

  }






  ngOnInit() {
  	this.alarmForm = new FormGroup({
        messageControl: new FormControl()
    });

    this.showRooms();
    this.showAllPatients();
  }

  sendAlarm(panel:any){

  	console.log("new alarm");

  	const alarm = <Alarm>({

       nurse_id : "",
       timestamp : "12345678",
       completed: false,
       allNurses: true,
       title: "Alarm",
       message: this.alarmForm.controls['messageControl'].value
     });

  	this.createNew(alarm, panel);

  }

  createNew(alarm: Alarm, panel: any) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");


    this.httpClient.post('http://'+this.webservice.baseURL+'/alarm/',alarm,{headers}).subscribe(
       val => {
           this.alarmForm.reset();
          panel.close()
      
            this.snackBar.open("Alarm created!", "", {
                  duration: 2000,
            });
        },
        response => { console.log("PUT call in error", response); },
        () => { console.log("The PUT observable is now completed.");}
    );

  }

  showRooms(){

    console.log(this.rooms[0]);


  }

   showAllPatients(){

     this.patients = this.httpClient.get<Patient[]>('http://'+this.webservice.baseURL+'/patients')  
  }

   getProfilePicture(id: String): String {
    return "http://"+this.webservice.baseURL+"/pictures/" + id + ".png";
  }

 


}
