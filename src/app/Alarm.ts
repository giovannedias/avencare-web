export interface Alarm {
  nurse_id: string;
  allNurses: boolean;
  timestamp: string;
  title: string;
  message: string;
  completed: boolean;
}

