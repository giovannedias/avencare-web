//import { BehaviorSubject } from 'rxjs';

interface Action {
  type: string;
}

export class TranslateString {
  private static instance: TranslateString;

  public language = "FR";

  //private actionsSubject = new BehaviorSubject<Action>(null);

  /*get actions$() {
    return this.actionsSubject.asObservable();
  }*/

  private constructor() {
  }

  static getInstance(): TranslateString {
    if (!TranslateString.instance) {
      TranslateString.instance = new TranslateString();
      let language = String(localStorage.getItem('language'));
      if (language == "") { language = "EN"; }
      TranslateString.instance.language = language

    }

    return TranslateString.instance;
  }


  public setLanguage(language: string) {
    localStorage.setItem('language', language);
  }
  public getTranslatedString(string: String): String {

    console.log("Language: " + this.language);


    if (this.language == "FR") {

      if (string == "New Task") { return "nouvelle tâche"}
      if (string == "NEW") { return "NOUVEAU"}
      if (string == "DELETE") { return "SUPPRIMER"}
      if (string == "EDIT") { return "MODIFIER"}
      if (string == "Home") { return "Accueil"}
      if (string == "Patients") { return "Patients"}
      if (string == "Nurses") { return "Infirmières"}
      if (string == "Tasks") { return "Tâches"}


    }

    return string;
  }

  /*dispatch(action: Action) {
    this.actionsSubject.next(action);
  }*/
}